from django.urls import path
from .import views

app_name = 'books'

urlpatterns = [
    path('', views.book_index, name='index'),               # books:index
    path('create/', views.book_create, name='create'),      # books:create
    path('edit/<int:id>', views.book_edit, name='edit'), # books:edit
    path('delete/<int:id>', views.book_delete, name='delete'),
    path('<int:id>', views.book_details, name='details')
]