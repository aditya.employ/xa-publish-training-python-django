from django.contrib import admin

# models class Book
from .models import Book

# Register your models here.
admin.site.register(Book)