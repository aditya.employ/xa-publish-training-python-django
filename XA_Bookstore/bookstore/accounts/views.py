from django.shortcuts import render, redirect

# import auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def accounts_signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('books:index')
    else:
        form = UserCreationForm()
    
    return render(request, 'accounts/signup.html', {'form' : form, 'title' : 'XA | Signup' } )

def accounts_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('books:index')
    else:
        form = AuthenticationForm()
    
    return render(request, 'accounts/login.html', {'form' : form, 'title' : 'XA | Login' } )

def accounts_logout(request):
    if request.method == 'POST':
        logout(request)
        return redirect('homepage')