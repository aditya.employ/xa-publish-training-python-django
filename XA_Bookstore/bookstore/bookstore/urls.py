from django.contrib import admin
from django.urls import path, include

# import view bookstore
from .import views

# import upload image cover
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Bookstore
    path('', views.homepage, name='homepage'),

    # Books
    path('books/', include('books.urls')),

    # Accounts
    path('accounts/', include('accounts.urls')),

    # App Admin
    path('admin/', admin.site.urls),
]

# for upload file image cover
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)