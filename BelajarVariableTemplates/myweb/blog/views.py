from django.shortcuts import render

# Create your views here.
def index(request):

    # variabel dict
    context = {
        # nama variable : nilai variable
        'title': 'XA | Blog',
        'judul' : 'Ini adalah halaman Blog menggunakan variable',
        "content" : """Lorem Ipsum is simply dummy text of the printing 
                    and typesetting industry. Lorem Ipsum has been the 
                    industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and 
                    scrambled it to make a type specimen book. It has 
                    survived not only five centuries, but also the leap 
                    into electronic typesetting, remaining essentially unchanged. 
                    It was popularised in the 1960s with the release of Letraset 
                    sheets containing Lorem Ipsum passages, and more recently with 
                    desktop publishing software like Aldus PageMaker including versions 
                    of Lorem Ipsum.""",
        """pengunjung""" : 199,
        'nav' : [
            # link , name / text
            ['/' , 'Home'],
            ['/blog', 'Blog']
        ]
    }

    return render(request, 'blog/index.html', context)