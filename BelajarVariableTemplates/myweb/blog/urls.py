from django.urls import path

# import views blog
from .import views

urlpatterns =  [
    # home blog
    path('', views.index)
]