from django.shortcuts import render

def index(request):
    # case 1 Dict
    context = {
        # Key       : Value Key
        # Variable  : Value Variable
        'title'     : 'XA | Home',
        'nav'       : [
                    # link , name / Text
                    ['/', 'Home'],
                    ['/blog', 'Blog']
        ],
        'nama'      : 'Dodi'
    }

    # print(type(context))

    # case 2 str
    # context = "XA | Home"
    # print(type(context))

    # # case 3 int
    # context = 90
    # print(type(context))

    return render(request, 'index.html', context) # context harus dict