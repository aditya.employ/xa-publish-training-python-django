from django.urls import path

# import views blog
from .import views

urlpatterns = [
    path('', views.index)
]