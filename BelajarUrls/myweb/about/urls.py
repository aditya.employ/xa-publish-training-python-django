from django.urls import path

# import views about
from .import views

urlpatterns = [
    path('', views.index)
]