from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return render(request, 'blog.html')

def contact(request):
    return HttpResponse("Ini adalah halaman contact blog")