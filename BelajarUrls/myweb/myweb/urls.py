from django.contrib import admin
from django.urls import path, include

# import views myweb
from . import views

# import views blog
# from blog import views as viewblog
# path('blog/', viewblog.index)

# import views about
# from about import views as viewsabout
# path('about/', viewsabout.index)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('blog/', include('blog.urls')),
    path('about/', include('about.urls'))
]
