from django.shortcuts import render

# Create your views here.
context = {
    'title' : 'XA | My Blog'
}

def index(request):

    return render(request, 'blog/index.html', context)