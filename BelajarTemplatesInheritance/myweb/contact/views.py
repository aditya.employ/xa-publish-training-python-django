from django.shortcuts import render

# Create your views here.
mydata = {
    'title' : 'XA | Contact'
}
def index(request):
    return render(request, 'contact/index.html', mydata)