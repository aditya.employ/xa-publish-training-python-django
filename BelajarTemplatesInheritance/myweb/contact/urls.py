from django.urls import path

# import views contact
from .import views

urlpatterns = [
    # home contact
    path('', views.index)
]