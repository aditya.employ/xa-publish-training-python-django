from django.shortcuts import render

# membuat method
def index(request):
    context = {
        'title'     : 'XA | Home'
    }
    return render(request, 'index.html', context)