from django.contrib import admin
from django.urls import path , include

# import views myweb
from .import views

urlpatterns = [
    path('admin/', admin.site.urls),

    # myweb
    path('', views.index),

    # blog
    path('blog/', include('blog.urls')), 

    # contact
    path('contact/', include('contact.urls')), 

]
