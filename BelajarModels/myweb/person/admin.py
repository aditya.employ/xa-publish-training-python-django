from django.contrib import admin

# import models app person
from .models import Person

# Register your models here.
admin.site.register(Person)